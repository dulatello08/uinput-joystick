#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <errno.h>
#include <linux/input.h>
#include <linux/uinput.h>

#define NAME "srv_socket"

int main(void)
{
  int sock;
  char buf[5];
  struct sockaddr_un server;
  //
  //the C program is recieving(??) the value of axis within unix domain socket  
  //
  int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK); //opening of uinput
  if (fd < 0) {
    printf("Opening of uinput failed!\n");
    return 1;
  }
  ioctl(fd, UI_SET_EVBIT, EV_KEY);
  ioctl(fd, UI_SET_KEYBIT, BTN_TL);
  ioctl(fd, UI_SET_KEYBIT, BTN_TR);
  ioctl(fd, UI_SET_KEYBIT, BTN_TL2);
  ioctl(fd, UI_SET_KEYBIT, BTN_TR2);
  ioctl(fd, UI_SET_KEYBIT, BTN_START);
  ioctl(fd, UI_SET_KEYBIT, BTN_SELECT);
  ioctl(fd, UI_SET_KEYBIT, BTN_THUMBL);
  ioctl(fd, UI_SET_KEYBIT, BTN_THUMBR);
  ioctl(fd, UI_SET_EVBIT, EV_ABS); //setting Gamepad thumbsticks
  ioctl(fd, UI_SET_ABSBIT, ABS_X);
  ioctl(fd, UI_SET_ABSBIT, ABS_Y);
  struct uinput_user_dev uidev; //setting the default settings of Gamepad
  memset(&uidev, 0, sizeof(uidev));
  snprintf(uidev.name, UINPUT_MAX_NAME_SIZE, "Simple Gamepad"); //Name of Gamepad
  uidev.id.bustype = BUS_USB;
  uidev.id.vendor  = 0x3;
  uidev.id.product = 0x3;
  uidev.id.version = 2;
  uidev.absmax[ABS_X] = 512; //Parameters of thumbsticks
  uidev.absmin[ABS_X] = -512;
  uidev.absfuzz[ABS_X] = 0;
  uidev.absflat[ABS_X] = 0;
  uidev.absmax[ABS_Y] = 512;
  uidev.absmin[ABS_Y] = -512;
  uidev.absfuzz[ABS_Y] = 0;
  uidev.absflat[ABS_Y] = 0;
  if(write(fd, &uidev, sizeof(uidev)) < 0) //writing settings
  {
    printf("error: write");
    return 1;
  }
  if(ioctl(fd, UI_DEV_CREATE) < 0) //writing ui dev create
  {
    printf("error: ui_dev_create");
    return 1;
  }
  //
  sock = socket(AF_UNIX, SOCK_STREAM, 0);
  server.sun_family = AF_UNIX;
  strcpy(server.sun_path, "srv_socket");
  connect(sock, (struct sockaddr *) &server, sizeof(struct sockaddr_un));
  //fcntl(sock, F_SETFL, O_NONBLOCK);
  //i decided to use blocking socket 

  //
  struct input_event ev;
  int value = 0;
  for(;;)
  {

    read(sock, buf, sizeof(buf));
    //
    //
    //
    strncpy(buf, buf, strlen(buf)-1);
    value = atoi(buf);
    printf("%d\n", value);
    //sleep(1); // no need to sleep it has to be fast
    memset(&ev, 0, sizeof(struct input_event)); //setting the memory for event
    ev.type = EV_ABS;
    ev.code = ABS_X;
    ev.value = value != 0 ? value : 0; //unnecesary but it works only like that
    write(fd, &ev, sizeof(struct input_event)); //writing the thumbstick change
    memset(&ev, 0, sizeof(struct input_event));
    ev.type = EV_SYN;
    ev.code = SYN_REPORT;
    ev.value = 0;
    if(write(fd, &ev, sizeof(struct input_event)) < 0) //writing the sync report
    {
      printf("error: sync-report");
      break;
    }//sync report is added because after i add more axis or button control it has to be sent in parallel without that it will fail
  }
  if(ioctl(fd, UI_DEV_DESTROY) < 0)
  {
    printf("error: ioctl");
    return 1;
  }
  close(fd);
  return 1;
}
