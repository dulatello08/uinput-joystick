def arduino_map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
#this is adapter code it gets value from serial maps it using math function and sends to unix domain socket 

import serial
import socket
import os, os.path
import time
import fcntl
from collections import deque

def prep():
    srv_sock = './srv_socket'
    try:
        os.unlink(srv_sock)
    except:
        if os.path.exists(srv_sock):
            raise
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    try:
        sock.bind(srv_sock)
        sock.listen(1)
        return sock
    except Exception as e:
        print(e)
        return False

ser = serial.Serial(
        # Serial Port to read the data from
        port='/dev/ttyACM0',

        #Rate at which the information is shared to the communication channel
        baudrate = 115200,

        #Applying Parity Checking (none in this case)
        parity=serial.PARITY_NONE,

       # Pattern of Bits to be read
        stopbits=serial.STOPBITS_ONE,

        # Total number of bits to be read
        bytesize=serial.EIGHTBITS,

        # Number of serial commands to accept before timing out
        timeout=1
)
sock=prep()
# Pause the program for 1 second to avoid overworking the serial port
while 1:
        x=ser.readline()
        if ('connection' not in locals()): 
            connection = sock.accept()[0]
        #connection.
        #x = int(-512 + (122457856*int(x))/133588455 + (11008*int(x)**2)/133588455) #using this function code is calibrating the output of joystick
        #this function will map the value from 0-1023 to -512 to 512
        x = int(-512 + (13545728*int(x))/14840661 + (1280*int(x)**2)/14840661)
        if(x==-1): x=0
        print(str(x))
        #now i will move the joystick (script is running)
        # it basically outputed the mapped serial output
        # it also sends the value to connected socket
        xStr = str(x) + "\n"
        connection.send(xStr.encode())
